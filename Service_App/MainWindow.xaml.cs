﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Globalization;
using System.Windows.Forms;


namespace Service_App
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //private int statusBarValue = 0;
        //private bool status = true;
        private string info ="";
        public MainWindow()
        {
            InitializeComponent();
            CultureInfo regionalInfo = CultureInfo.CurrentCulture;

            var c = regionalInfo.NumberFormat.CurrencyGroupSeparator;
            if (regionalInfo.NumberFormat.CurrencyDecimalSeparator != ",")
            {
                System.Windows.MessageBox.Show("Zły znak -> Symbol dziesiętny:" + regionalInfo.NumberFormat.CurrencyGroupSeparator + "\n Powinien być -> , (przecinek) \n Zmien w ustawieniach systemu");
            }
            //if(regionalInfo.NumberFormat.CurrencyGroupSeparator!= " ")
            //{
            //    System.Windows.MessageBox.Show("Zły znak Symbol grupowania cyfr" + regionalInfo.NumberFormat.CurrencyGroupSeparator + "\n Powinien być -> pusty/spacja\n Zmien w ustawieniach systemu");
            //}
            //System.Windows.MessageBox.Show("Current digital sparator " + regionalInfo.NumberFormat.CurrencyGroupSeparator);

        }

        private void updateProgessBar(int valueToUpdate)
        {

            //TODO add value;
        }

        private void btnOpenFile_Click(object sender, RoutedEventArgs e)
        {
            string filePath = null;
            string folderPath = null;

            if (rbFileOpen.IsChecked == true)
            {
                // Create OpenFileDialog
                Microsoft.Win32.OpenFileDialog openFileDlg = new Microsoft.Win32.OpenFileDialog
                {
                    InitialDirectory = AppDomain.CurrentDomain.BaseDirectory, 
                    //InitialDirectory = @"D:\rppcc_dane\cos_z_robotem",
                    //File filter
                    Filter = "zip files (*.zip)|*zip|All files (*.*)|*.*",
                    //default choice of file filter
                    FilterIndex = 1
                };

                // Launch OpenFileDialog by calling ShowDialog method
                Nullable<bool> result = openFileDlg.ShowDialog();
                if (result == true)
                {
                    filePath = openFileDlg.FileName;
                    // Get the selected file name and display in a TextBox.
                    // Load content of file in a TextBlock
                    info += "Otworzono plik: " + filePath;
                    txtbStatusInfo.Text = info;
                    txtbFilePath.Text = filePath;

                    var fp = new FileProcessing(filePath, folderPath, txtbStatusInfo);
                    //status = fp.extractFile();
                    //if(status == true)
                    //{
                    //    info += " \n Rozpakowano pliki pomyślnie";
                    //    txtbStatusInfo.Text = info;
                    //    info += " \n Wyszukiwanie wartości...\n generowanie pliku Excel... \n może zająć chwilkę! ";
                    //    txtbStatusInfo.Text = info;
                    //}
                    //status = fp.processFile();
                    

                }
            }
            else if (rbPathOpen.IsChecked == true)
            {
                FolderBrowserDialog openFolderDlg = new FolderBrowserDialog();
                openFolderDlg.RootFolder = Environment.SpecialFolder.MyComputer;
                openFolderDlg.Description = "Wybierz folder z plikami";
                openFolderDlg.ShowNewFolderButton = false;

                if (openFolderDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    folderPath = openFolderDlg.SelectedPath;
                    txtbFilePath.Text = folderPath;
                    txtbStatusInfo.Text = "wybrano sciezke: " + folderPath;
                    info += " \n Wyszukiwanie wartości...\n generowanie pliku Excel... \n może zająć chwilkę! ";
                    txtbStatusInfo.Text = info;
                    FileProcessing fp = new FileProcessing(filePath, folderPath,txtbStatusInfo);
                    //status = fp.processFile();
                    //if (status == true)
                    //{
                    //    //info += " \n Wyszukiwanie wartości...\n generowanie pliku Excel... ";
                    //    //txtbStatusInfo.Text = info;
                    //    status = fp.generateExcelFile();
                    //    System.Windows.Forms.MessageBox.Show("Wygenrowano poprawnie plik");
                    //    if (status == true)
                    //    {
                    //        info += " \n Zakończono genrowanie ";
                    //        txtbStatusInfo.Text = info;
                    //    }
                    //}
                    //else
                    //{
                    //    info += " \n Nie wygenrowano poprawnie pliku ";
                    //    txtbStatusInfo.Text = info;
                    //    System.Windows.Forms.MessageBox.Show("Nie wygenrowano poprawnie pliku");
                    //}
                   
                    

                }

            }
            else
            {
                System.Windows.Forms.MessageBox.Show("Nie wybrano opcji");
            }

        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            string fileExt = ".xlsx";
            DirectoryInfo di = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
            FileInfo[] files = di.GetFiles("*" + fileExt).Where(p => p.Extension == fileExt).ToArray();
            foreach (FileInfo file in files)
                try
                {
                    file.Attributes = FileAttributes.Normal;
                    File.Delete(file.FullName);
                }
                catch (Exception er)
                {
                    System.Windows.Forms.MessageBox.Show("Upss:" + er);
                }
        }
    }
}
