﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO.Compression;
using System.IO;
using System.Windows;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.Globalization;
using System.Windows.Controls;

namespace Service_App
{
    class FileProcessing
    {
        private string filesPath;
        private string actualDirectory;
        private string extractDirectory;
        private string[] filePaths;
        private string[] globalPointsFile;
        private Dictionary<string, string> pointsDictonary = new Dictionary<string, string>();
        private Dictionary<string, string> pointsDictonaryHDL = new Dictionary<string, string>();
        private string infoRobotLine;
        private string infoRobotStation;
        private string infoRobotRobotNo;
        struct dataToExcel
        {
            public string line;
            public string station;
            public string robot;
            public string programNo;
            public string pointNo;
            public double pointX;
            public double pointY;
            public double pointZ;
            public double pointHDL_Z;
            public double pointHDL_X;
            public double pointHDL_Y;
            public string Comment;
        }

        private List<dataToExcel> dataTo = new List<dataToExcel>();
        struct srcFiles
        {
            public string robotName;
            public string srcPath;
            public string datPath;
            public string globalPointsPath;
        }
        private List<srcFiles> src = new List<srcFiles>();
        private List<string> srcFile = new List<string>();
        private List<AllPointsInfo> allPoints = new List<AllPointsInfo>();
        private string programName;
        private List<string> points = new List<string>();
        private List<string> globalPoints = new List<string>();
        private string infoRobotComment;
        private string HdlfileDirPath;
        private string[] filesHDLPaths;
        private string info;
        MainWindow mainWind = Application.Current.Windows.Cast<Window>().FirstOrDefault(window => window is MainWindow) as MainWindow;

        private List<dataToExcel> DataTo
        {
            get
            {
                return dataTo;
            }

            set
            {
                dataTo = value;
            }
        }
        public FileProcessing(string filesPath, string folderPath, TextBlock info)
        {
            HdlfileDirPath = AppDomain.CurrentDomain.BaseDirectory + "hdl\\";
            this.filesPath = string.IsNullOrEmpty(filesPath) ? folderPath : filesPath;
            actualDirectory = AppDomain.CurrentDomain.BaseDirectory;
            extractDirectory = string.IsNullOrEmpty(filesPath) ? folderPath : actualDirectory + "data\\";
            this.info = info.Text;
            


            processFiles(this.filesPath, folderPath);
        }
        public void updateTextForm(string text)
        {
            info += text;
            mainWind.txtbStatusInfo.Text = info;
        }
        public void processFiles(string filesPath, string folderPath)
        {
            bool status = false;
            int numOfSWPFiles = 0;
            //extract files from .zip
            if (folderPath != filesPath)
            {
                status = extractFile();
            }

            if (searchSrcFiles(filesPath, ".src") > 0)
            {
                assignData();
                status = true;
            }


            try
            {
                numOfSWPFiles = searchFiles("SWP", "*.src", "Global", "*.dat");
                if (numOfSWPFiles > 1 && chcekDirectoryExistAndNoEmpty(HdlfileDirPath) == true)
                {
                    addPointsToDictonary();
                    searchHDLFiles(HdlfileDirPath);
                    foreach (var file in filePaths)
                    {
                        //findRobotLine(file);
                        //findRobotStation(file);
                        //findRobotNo(file);
                        //findRobotProgramName(file);

                        getInfomationAboutRobot(file);
                        findPointsInFile(file, globalPointsFile[0].ToString());
                    }
                    //return true;
                }
                else
                {
                    MessageBox.Show("Nie znaleziono plików z punktami.");
                    updateTextForm("Nie znaleziono plików z punktami.");
                    // return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Kod błedu: " + e.ToString());
                //return false;
            }

            if (status == true)
            {
                //info += " \n Wyszukiwanie wartości...\n generowanie pliku Excel... ";
                //txtbStatusInfo.Text = info;
                status = generateExcelFile(numOfSWPFiles);
                System.Windows.Forms.MessageBox.Show("Wygenrowano poprawnie plik");
                if (status == true)
                {
                    updateTextForm(" \n Zakończono genrowanie ");

                    //txtbStatusInfo.Text = info;
                }
            }
            else
            {
                updateTextForm(" \n Nie wygenrowano poprawnie pliku ");
                // txtbStatusInfo.Text = info;
                System.Windows.Forms.MessageBox.Show("Nie wygenrowano poprawnie pliku");
            }
        }


        private int findRobotNo(string fileToFind)
        {
            string[] data;
            //int count = 0;
            string fileLine;
            char[] separators = new char[] { '(', ',', ')', '{', '}', ' ' };

            StreamReader file = new StreamReader(fileToFind);
            while ((fileLine = file.ReadLine()) != null)
            {

                data = fileLine.Replace('=', ' ').Replace(';', ' ').Split(separators);

                for (var i = 0; i < data.Length; i++)
                {

                    //string patern = "RB_";

                    if (data[i].ToString() == "Anlagenbezeichn:")
                    {
                        string[] line_temp;

                        line_temp = data[i + 1].Replace("RB", " ").Split('_', ' ');
                        if (line_temp.Length >= 2)
                        {
                            infoRobotRobotNo = line_temp[2];
                            return int.Parse(infoRobotRobotNo) / 100;
                        }

                    }
                    if (data[i].ToString() == "Stations-Nr:")
                    {
                        string[] station_temp;

                        station_temp = data[i + 1].Replace("RB", " ").Split('_', ' ');
                        if (station_temp.Length >= 2)
                        {
                            infoRobotRobotNo = station_temp[2];
                            return int.Parse(infoRobotRobotNo) / 100;
                        }
                    }
                }
            }
            return -1;

        }

        private string findRobotStation(string fileToFind)
        {
            string[] data;
            //int count = 0;
            string fileLine;
            char[] separators = new char[] { '(', ',', ')', '{', '}', ' ' };

            StreamReader file = new StreamReader(fileToFind);
            while ((fileLine = file.ReadLine()) != null)
            {

                data = fileLine.Replace('=', ' ').Replace(';', ' ').Split(separators);

                for (var i = 0; i < data.Length; i++)
                {
                    //string patern = "RB_";
                    if (data[i].ToString() == "Anlagenbezeichn:")
                    {
                        string[] line_temp;

                        line_temp = data[i + 1].Replace("RB", " ").Split('_', ' ');
                        if (line_temp.Length >= 2)
                        {
                            infoRobotStation = line_temp[0];
                            return infoRobotStation;
                        }
                    }
                    if (data[i].ToString() == "Stations-Nr:")
                    {
                        string[] station_temp;

                        station_temp = data[i + 1].Replace("RB", " ").Split('_', ' ');
                        if (station_temp.Length >= 2)
                        {
                            infoRobotStation = station_temp[0];
                            return infoRobotStation;
                        }
                    }
                }
            }
            return infoRobotStation;
        }

        private void searchHDLFiles(string dirPath)
        {
            string fileExtension = "*.*";
            filesHDLPaths = Directory.GetFiles(dirPath, fileExtension, SearchOption.AllDirectories).Where(s => s.EndsWith(".xls") || s.EndsWith(".xlsx")).ToArray();
            foreach (var file in filesHDLPaths)
            {
                addPointsToDictorayHDL(file);
            }

        }
        private int searchSrcFiles(string dirPath, string extension)
        {
            srcFiles data = new srcFiles();
            string filter = "Inbetriebn";
            string[] temp;
            string[] fileSRC;
            string fileExtension = extension;
            fileSRC = Directory.GetFiles(dirPath, "*" + extension, SearchOption.AllDirectories).Where(s => s.EndsWith(extension) == true && s.Contains(filter) == false).ToArray();
            foreach (var file in fileSRC)
            {
                temp = file.Split('.');
                data.robotName = temp[0];
                data.srcPath = file;
                data.datPath = temp[0] + ".dat";
                 
                data.globalPointsPath = File.Exists((Path.GetDirectoryName(file) + "\\Global_Points.dat"))? Path.GetDirectoryName(file)+"\\Global_Points.dat": null;
                src.Add(data);
            }
            return src.Count;
        }
        private void assignData()
        {
            foreach (var file in src)
            {
                AllPointsInfo pointsInfo = new AllPointsInfo();
                pointsInfo.Line = findRobotLine(file.srcPath);
                pointsInfo.Station = findRobotStation(file.srcPath);
                pointsInfo.Robot = findRobotNo(file.srcPath);
                pointsInfo.ProgramNo = findRobotProgramName(file.srcPath);
                //; FOLD PTP transferpoint_3A Vel = 100 % DEFAULT;%{ PE}

                char[] separators = new char[] { ' ', '=', ':' };
                string line;
                string ln;
                string[] info;
                string[] data;
                bool add = false;
                bool endfold = false;
                Dictionary<string, string> datFile = new Dictionary<string, string>();
                StreamReader datInfo = new StreamReader(file.datPath);
                {
                    //string[] filter = { "DECL", "FDAT", "PDAT", "E6POS", "ENDDAT" };
                    while ((ln = datInfo.ReadLine()) != null)
                    {
                        info = ln.Replace("DECL", " ").Replace(" FDAT ", " ").Replace(" PDAT ", " ").Replace(" LDAT ", " ").Replace(" E6POS ", " ").Replace("ENDDAT", " ").TrimStart().Split('=');
                        if (info.Length > 1)
                        {
                            try
                            {
                                if (datFile.ContainsKey(info[0]) == false)
                                {
                                    datFile.Add(info[0], info[1]);
                                }

                            }
                            catch (ArgumentException)
                            {
                                MessageBox.Show("Blad:" + info[0] + "istnije w pliku wiecej razy: " + file.srcPath);
                            }

                        }
                    }
                }
                if (file.globalPointsPath != null)
                {
                    StreamReader globalDatInfo = new StreamReader(file.globalPointsPath);
                    {
                        //string[] filter = { "DECL", "FDAT", "PDAT", "E6POS", "ENDDAT" };
                        while ((ln = globalDatInfo.ReadLine()) != null)
                        {
                            info = ln.Replace("DECL", " ").Replace(" GLOBAL ", " ").Replace(" E6POS ", " ").Replace("ENDDAT", " ").Trim().Split('=');
                            if (info.Length > 1)
                            {
                                try
                                {
                                    if (datFile.ContainsKey(info[0]) == false)
                                    {
                                        datFile.Add(info[0], info[1]);
                                    }

                                }
                                catch (ArgumentException)
                                {
                                    MessageBox.Show("Blad:" + info[0] + "istnije w pliku wiecej razy: " + file.srcPath);
                                }

                            }
                        }
                    }
                }
                StreamReader files = new StreamReader(file.srcPath);
                while ((line = files.ReadLine()) != null)
                {
                    data = line.Replace(';', ' ').Replace('[', ' ').Replace(']', ' ').TrimStart().Split(separators);
                    if (data.Length >= 1)
                    {
                        switch (data[0].ToString())
                        {
                            case "FOLD":
                                if (data[1] == "PTP")
                                {
                                    pointsInfo.PTP_LIN1 = data[1];
                                    pointsInfo.PointName = data[2].Replace("<", " ").Replace(">", " ").Trim();
                                    //add = true;
                                }
                                else if (data[1] == "LIN")
                                {
                                    pointsInfo.PTP_LIN1 = data[1];
                                    pointsInfo.PointName = data[2].Replace("<", " ").Replace(">", " ").Trim();
                                }
                                break;
                            case "PDAT_ACT":
                                if (data.Length <= 2)
                                {
                                    pointsInfo.PDAT_LDAT1 = data[1];
                                }
                                else
                                {
                                    pointsInfo.PDAT_LDAT1 = data[3];
                                }
                                break;
                            case "LDAT_ACT":
                                if (data.Length <= 2)
                                {
                                    pointsInfo.PDAT_LDAT1 = data[1];
                                }
                                else
                                {
                                    pointsInfo.PDAT_LDAT1 = data[3];
                                }
                                break;
                            case "FDAT_ACT":
                                if (data.Length <= 2)
                                {
                                    pointsInfo.FDAT1 = data[1];
                                }
                                else if (data.Length > 3)
                                {
                                    pointsInfo.FDAT1 = data[3];
                                }
                                break;
                            case "PTP":
                                pointsInfo.E6POS1 = data[1];
                                endfold = true;
                                break;
                            case "LIN":
                                pointsInfo.E6POS1 = data[1];
                                endfold = true;
                                break;
                            case "SET_CD_PARAMS":
                                add = true;
                                break;
                            default:
                                break;
                        }
                        if (data.Length >= 4)
                        {

                            if (data[3] == "Vel")
                            {
                                pointsInfo.Vel1 = data[4];
                            }

                            if (data.Length >= 11 && data[3] == "CONT")
                            {
                                pointsInfo.CONT1 = "YES";
                                if (data[4] == "Vel")
                                {
                                    pointsInfo.Vel1 = data[5];
                                }
                                if (data[8] == "Tool")
                                {
                                    pointsInfo.PDAT_LDAT1 = data[7];
                                    pointsInfo.ToolNo = data[9];
                                    pointsInfo.ToolName = data[11];
                                    pointsInfo.BaseNo = data[13];
                                    pointsInfo.BaseName = data[15];
                                }
                                else if (data[7] == "Tool")
                                {
                                    pointsInfo.PDAT_LDAT1 = data[6];
                                    pointsInfo.ToolNo = data[8];
                                    pointsInfo.ToolName = data[9];
                                    pointsInfo.BaseNo = data[10];
                                    pointsInfo.BaseName = data[12];
                                }
                            }
                            else if (data.Length >= 11)
                            {
                                if (data[3] == "Vel")
                                {
                                    pointsInfo.Vel1 = data[4] + " " + data[5];
                                }
                                if (data[7] == "Tool")
                                {
                                    pointsInfo.PDAT_LDAT1 = data[6];
                                    pointsInfo.ToolNo = data[8];
                                    pointsInfo.ToolName = data[10];
                                    if (data.Length >= 12)
                                    {
                                        pointsInfo.BaseNo = data[12];
                                    }

                                    if (data.Length >= 15)
                                    {
                                        pointsInfo.BaseName = data[14];
                                    }
                                }
                            }
                        }
                        if (add == true && endfold == true)
                        {
                            char[] filter = { ' ', ',' };
                            if (datFile.ContainsKey(pointsInfo.PointName))
                            {

                            }
                            if (datFile.ContainsKey(pointsInfo.PDAT_LDAT1))
                            {
                                string[] tmp_ = datFile[pointsInfo.PDAT_LDAT1].Replace("{", " ").Replace("}", " ").Trim().Split(filter);
                                pointsInfo.Vel2 = tmp_[1];
                                pointsInfo.Acc = tmp_[3];
                                pointsInfo.ApoDiststring = tmp_[5];
                                pointsInfo.ApoModestring = tmp_[7];
                                pointsInfo.GearJerkstring = tmp_[9];
                                pointsInfo.Expax = tmp_[11];

                            }
                            if (datFile.ContainsKey(pointsInfo.FDAT1))
                            {
                                string[] tmp_ = datFile[pointsInfo.FDAT1].Replace("{", " ").Replace("}", " ").Trim().Split(filter);
                                pointsInfo.Tool1 = tmp_[1];
                                pointsInfo.Base1 = tmp_[3];
                                pointsInfo.IpoFrame = tmp_[5];
                            }
                            if (datFile.ContainsKey(pointsInfo.E6POS1))
                            {
                                string[] tmp_ = datFile[pointsInfo.E6POS1].Replace("{", " ").Replace("}", " ").Trim().Split(filter);
                                pointsInfo.X_A11 = tmp_[1];
                                pointsInfo.Y_A21 = tmp_[3];
                                pointsInfo.Z_A31 = tmp_[5];
                                pointsInfo.A_A41 = tmp_[7];
                                pointsInfo.B_A51 = tmp_[9];
                                pointsInfo.C_A61 = tmp_[11];
                                pointsInfo.S1 = tmp_[13];
                                pointsInfo.T1 = tmp_[15];
                                pointsInfo.E11 = tmp_[17];
                                pointsInfo.E21 = tmp_[19];
                                pointsInfo.E31 = tmp_[21];
                                pointsInfo.E41 = tmp_[23];
                                pointsInfo.E51 = tmp_[25];
                                pointsInfo.E61 = tmp_[27];
                            }
                            //add all
                            AllPointsInfo tmp = pointsInfo.CreateCopy();
                            allPoints.Add(tmp);

                            pointsInfo.ToolNo = null;
                            pointsInfo.ToolName = null;
                            pointsInfo.BaseNo = null;
                            pointsInfo.BaseName = null;
                            pointsInfo.Vel1 = null;
                            pointsInfo.CONT1 = null;
                            pointsInfo.E6POS1 = null;
                            pointsInfo.FDAT1 = null;
                            pointsInfo.PDAT_LDAT1 = null;
                            pointsInfo.PTP_LIN1 = null;
                            pointsInfo.PointName = null;
                            pointsInfo.Vel2 = null;
                            pointsInfo.Acc = null;
                            pointsInfo.ApoDiststring = null;
                            pointsInfo.ApoModestring = null;
                            pointsInfo.GearJerkstring = null;
                            pointsInfo.Expax = null;
                            pointsInfo.Tool1 = null;
                            pointsInfo.Base1 = null;
                            pointsInfo.IpoFrame = null;
                            pointsInfo.X_A11 = null;
                            pointsInfo.Y_A21 = null;
                            pointsInfo.Z_A31 = null;
                            pointsInfo.A_A41 = null;
                            pointsInfo.B_A51 = null;
                            pointsInfo.C_A61 = null;
                            pointsInfo.S1 = null;
                            pointsInfo.T1 = null;
                            pointsInfo.E11 = null;
                            pointsInfo.E21 = null;
                            pointsInfo.E31 = null;
                            pointsInfo.E41 = null;
                            pointsInfo.E51 = null;
                            pointsInfo.E61 = null;

                            add = false;
                            endfold = false;
                        }
                    }
                }
            }
        }

        private void clearData()
        {
            infoRobotComment = "";
            points.Clear();
            globalPoints.Clear();
        }

        private void addPointsToDictonary()
        {
            string wordToFind = "E6POS";
            char[] separators = new char[] { '(', ',', ')', '=', '{', '}', ' ' };
            string line;
            string[] data;
            int count = 0;
            //MessageBox.Show("Plik z punktami:" + fileToFind);
            foreach (var fileWithPoints in globalPointsFile)
            {

                StreamReader file = new StreamReader(fileWithPoints);
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Length > 30)
                    {
                        //  MessageBox.Show("znalazlem: min 150");
                        data = line.Replace('=', ' ').Split(separators);
                        if (data.Length > 30)
                        {
                            //    MessageBox.Show("znalazlem: min 33");
                            if (data[2].ToString() == wordToFind)
                            {
                                string cord = data[6].ToString() + ";" + data[8].ToString() + ";" + data[10].ToString();
                                pointsDictonary.Add(data[3].ToString(), cord);
                                count++;
                            }
                        }
                    }
                }
            }
        }

        private string findRobotProgramName(string fileToFind)
        {
            string wordToFind = "DEF";
            string line;
            string[] data;

            StreamReader file = new StreamReader(fileToFind);
            while ((line = file.ReadLine()) != null)
            {
                data = line.Split();
                if (data[0].ToString() == wordToFind)
                {
                    //Asign of program name to List of progrmas
                    programName = data[1].ToString().Substring(0, data[1].Length - 1);
                    //   MessageBox.Show("Znaleziono nazwe programu: " + name);
                    return programName;
                }
            }
            return programName;
        }

        private string findRobotLine(string fileToFind)
        {
            string[] data;
            //int count = 0;
            string fileLine;
            char[] separators = new char[] { '(', ',', ')', '{', '}', ' ' };

            StreamReader file = new StreamReader(fileToFind);
            while ((fileLine = file.ReadLine()) != null)
            {

                data = fileLine.Replace('=', ' ').Replace(';', ' ').Split(separators);

                for (var i = 0; i < data.Length; i++)
                {
                    string patern = "RB_";

                    if (data[i].ToString() == "Anlagenbezeichn:")
                    {
                        string[] line_temp;
                        if (data[i + 1].IndexOf(patern) == 0)
                        {
                            line_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = line_temp[1];

                            return infoRobotLine;
                        }
                        else if (data[i + 1].IndexOf(patern) == -1)
                        {
                            line_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = line_temp[0];

                            return infoRobotLine;
                        }

                    }
                    if (data[i].ToString() == "Stations-Nr:")
                    {
                        string[] station_temp;
                        if (data[i + 1].IndexOf(patern) == 0)
                        {
                            station_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = station_temp[1];
                            return infoRobotLine;
                        }
                        else if (data[i + 1].IndexOf(patern) == -1)
                        {
                            station_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = station_temp[0];
                            return infoRobotLine;
                        }
                    }
                }
            }
            return infoRobotLine;
        }
        private void getInfomationAboutRobot(string fileToFind)
        {
            /*info are asigned to infoRobot[]:  
            * 0 - LINE
            * 1 - STATION
            * 2 - ROBOT NO
            */
            string[] data;
            //int count = 0;
            string fileLine;
            char[] separators = new char[] { '(', ',', ')', '{', '}', ' ' };

            StreamReader file = new StreamReader(fileToFind);

            while ((fileLine = file.ReadLine()) != null)
            {

                data = fileLine.Replace('=', ' ').Replace(';', ' ').Split(separators);

                for (var i = 0; i < data.Length; i++)
                {

                    string patern = "RB_";

                    if (data[i].ToString() == "Anlagenbezeichn:")
                    {
                        string[] line_temp;
                        if (data[i + 1].IndexOf(patern) == 0)
                        {
                            line_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = line_temp[1];
                        }
                        else if (data[i + 1].IndexOf(patern) == -1)
                        {
                            line_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = line_temp[0];
                        }
                        else
                        {
                            line_temp = data[i + 1].Replace("RB", " ").Split('_', ' ');
                            if (line_temp.Length >= 2)
                            {
                                infoRobotStation = line_temp[0];
                                infoRobotRobotNo = line_temp[2];
                            }
                        }
                    }
                    if (data[i].ToString() == "Stations-Nr:")
                    {
                        string[] station_temp;
                        if (data[i + 1].IndexOf(patern) == 0)
                        {
                            station_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = station_temp[1];
                        }
                        else if (data[i + 1].IndexOf(patern) == -1)
                        {
                            station_temp = data[i + 1].Split('_', ' ');
                            infoRobotLine = station_temp[0];
                        }
                        else
                        {
                            station_temp = data[i + 1].Replace("RB", " ").Split('_', ' ');
                            if (station_temp.Length >= 2)
                            {
                                infoRobotStation = station_temp[0];
                                infoRobotRobotNo = station_temp[2];
                            }
                        }

                    }
                }
            }

        }
        private void findPointsInFile(string fileToFind, string fileToFindGlobalPoints)
        {
            string wordToFind = "DSG_GetPara";
            string pointPrefix = "gXSWPMRA2";
            char[] separators = new char[] { '(', ',', ')' };
            string line;
            string[] data;
            var dt = new dataToExcel();

            StreamReader file = new StreamReader(fileToFind);
            while ((line = file.ReadLine()) != null)
            {
                data = line.Split(separators);
                if (data.Length > 1)
                {
                    //if linie have wordToFind - line with parameters
                    if (data[0].ToString() == wordToFind)
                    {
                        //check that line have points to correct process -> pointPrefix
                        string[] pointCheck = data[1].Split('_');
                        if (pointCheck[0] == pointPrefix)
                        {
                            string pointName = data[1].ToString();

                            points.Add(pointName);
                            findPoints(pointName);
                            DataTo.Add(assignValues(dt));
                        }
                        //skip that point
                        else
                        {
                            clearData();
                        }

                    }
                }

            }
        }
        private dataToExcel assignValues(dataToExcel dt)
        {
            dt.line = (infoRobotLine != null) ? infoRobotLine : "Miss";
            dt.station = (infoRobotStation != null) ? infoRobotStation : "Miss";
            dt.robot = (infoRobotRobotNo != null) ? infoRobotRobotNo : "Miss";  // possible Bug with wrong number of robot 
            dt.programNo = programName;
            dt.pointNo = points.Count > 0 ? points[0].Remove(0, 2) : "Miss";
            if (globalPoints.Count == 6)
            {
                dt.pointX = (globalPoints[0] != null || globalPoints[0] != 0.ToString()) ? Convert.ToDouble(globalPoints[0].ToString().Replace('.', ',')) : 0;
                dt.pointY = (globalPoints[1] != null || globalPoints[1] != 0.ToString()) ? Convert.ToDouble(globalPoints[1].ToString().Replace('.', ',')) : 0;
                dt.pointZ = (globalPoints[2] != null || globalPoints[2] != 0.ToString()) ? Convert.ToDouble(globalPoints[2].ToString().Replace('.', ',')) : 0;
                dt.pointHDL_X = (globalPoints[3] != null || globalPoints[3] != 0.ToString()) ? Convert.ToDouble(globalPoints[3].ToString().Replace('.', ',')) : 0;
                dt.pointHDL_Y = (globalPoints[4] != null || globalPoints[4] != 0.ToString()) ? Convert.ToDouble(globalPoints[4].ToString().Replace('.', ',')) : 0;
                dt.pointHDL_Z = (globalPoints[5] != null || globalPoints[5] != 0.ToString()) ? Convert.ToDouble(globalPoints[5].ToString().Replace('.', ',')) : 0;
            }
            else if (globalPoints.Count == 3)
            {
                dt.pointX = (globalPoints[0] != null || globalPoints[0] != 0.ToString()) ? Convert.ToDouble(globalPoints[0].ToString().Replace('.', ',')) : 0;
                dt.pointY = (globalPoints[1] != null || globalPoints[1] != 0.ToString()) ? Convert.ToDouble(globalPoints[1].ToString().Replace('.', ',')) : 0;
                dt.pointZ = (globalPoints[2] != null || globalPoints[2] != 0.ToString()) ? Convert.ToDouble(globalPoints[2].ToString().Replace('.', ',')) : 0;
                dt.pointHDL_X = Convert.ToDouble(0.ToString());
                dt.pointHDL_Y = Convert.ToDouble(0.ToString());
                dt.pointHDL_Z = Convert.ToDouble(0.ToString());
                infoRobotComment = "Missing HDL info about point: " + dt.pointNo;
            }
            else
            {
                dt.pointX = Convert.ToDouble(0.ToString());
                dt.pointY = Convert.ToDouble(0.ToString());
                dt.pointZ = Convert.ToDouble(0.ToString());
                dt.pointHDL_X = Convert.ToDouble(0.ToString());
                dt.pointHDL_Y = Convert.ToDouble(0.ToString());
                dt.pointHDL_Z = Convert.ToDouble(0.ToString());
                infoRobotComment = "Missing info about point: " + dt.pointNo;
            }
            dt.Comment = infoRobotComment != null ? infoRobotComment : " ";
            clearData();
            return dt;
        }
        private void findPoints(string pointName)
        {
            if (pointsDictonary.ContainsKey(pointName) == true)
            {
                string pointsDic = pointsDictonary[pointName].ToString();
                string[] values = pointsDic.Split(';');
                globalPoints.Add(values[0].ToString());
                globalPoints.Add(values[1].ToString());
                globalPoints.Add(values[2].ToString());

            }
            if (pointsDictonaryHDL.ContainsKey(pointName) == true)
            {
                string pointsHDL = pointsDictonaryHDL[pointName].ToString();
                string[] values = pointsHDL.Split(';');
                globalPoints.Add(values[0].ToString());
                globalPoints.Add(values[1].ToString());
                globalPoints.Add(values[2].ToString());
            }

        }
        /// <summary>
        /// function to search files with SWP points ang Global Point config
        /// </summary>
        /// <param name="templateProces"></param>
        /// <param name="templateExtension"></param>
        /// <param name="pointFile"></param>
        /// <param name="pointExtension"></param>
        /// <returns>numOfSearchedItems</returns>
        private int searchFiles(string templateProces, string templateExtension, string pointFile, string pointExtension)
        {
            int numOfSearchedItems = 0;
            //Search files with robot cfg
            //if
            filePaths = Directory.GetFiles(extractDirectory, templateExtension, SearchOption.AllDirectories).Where(s => s.Contains(templateProces) == true).ToArray();
            //TODO:  if not found global points exception ?

            //Search file with global points
            globalPointsFile = Directory.GetFiles(extractDirectory, pointExtension, SearchOption.AllDirectories).Where(s => s.Contains(pointFile) == true).ToArray();

            numOfSearchedItems += filePaths.Length + globalPointsFile.Length;
            //Debug info of returned items
            MessageBox.Show("wyszukiwanie plikow zakończone, znaleziono: " + numOfSearchedItems, "searchFiles");

            return numOfSearchedItems;
        }
        public void addPointsToDictorayHDL(string fileName)
        {
            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            int rw = 0;
            int cl = 0;
            int startFromRow = 7;

            xlApp = new Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(fileName, 0, true, 5, "", "", true, Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            string currentSheet = "11 Wid.-Pkt.Schweißen";
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(currentSheet);

            range = xlWorkSheet.UsedRange;
            rw = range.Rows.Count;
            cl = range.Columns.Count;

            //int cou = 1;
            for (var i = startFromRow; i < rw; i++)
            {
                string point = null;
                string coordinates = null;
                string prefix = "gXSWPMRA2_";
                if (!string.IsNullOrEmpty(Convert.ToString((range.Cells[i, 1] as Excel.Range).Value2)))
                {
                    //cou++;
                    point = prefix + (string)(range.Cells[i, 1] as Excel.Range).Value2.ToString();
                    coordinates = (string)(range.Cells[i, 3] as Excel.Range).Value2.ToString()
                        + ";" + (string)(range.Cells[i, 4] as Excel.Range).Value2.ToString()
                        + ";" + (string)(range.Cells[i, 5] as Excel.Range).Value2.ToString();
                    coordinates = coordinates.Replace(',', '.');

                    if (!pointsDictonaryHDL.ContainsKey(point))
                    {
                        pointsDictonaryHDL.Add(point, coordinates);
                    }
                    else
                    {
                        pointsDictonaryHDL[point] = coordinates;
                    }

                }
                else
                {
                    break;
                }

            }

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
        }

        public bool generateExcelFile(int numOfSWPFiles)
        {
            try
            {
                Excel.Application xlApp = new Excel.Application();

                if (xlApp == null)
                {
                    MessageBox.Show("Excel is not properly installed!!");

                }
                xlApp.DisplayAlerts = false;
                xlApp.ErrorCheckingOptions.BackgroundChecking = false;



                Excel.Workbook xlWorkBook;
                Excel.Worksheet xlWorkSheet;
                object misValue = System.Reflection.Missing.Value;

                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = xlWorkBook.Worksheets.Add(misValue);
                xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);
                xlWorkSheet.Name = "All Points";
                //ALL POINTS:
                string[] headerAllPoints = { "Line", "Station", "Robot", "Program No", "Point Name", "PTP/LIN", "CONT" , "Vel", "Tool No", "Tool Name", "Base No",
                                            "Base Name","extTCP","PDAT & LDAT","FDAT","E6POS"," ","Global type","X / A1", "Y / A2","Z / A3"," ","A / A4","B / A5","C / A6",
                                            "S","T","E1","E2","E3","E4","E5","E6","Tool","Base","Ipo Frame","Vel","Acc","Apo Dist", "Apo Mode", "Gear Jerk",
                                            "Expax Ign","Apo Fac", "Axis Vel","Axis Acc","Comment Optimization","Refresh Point"};
                for (var i = 0; i < headerAllPoints.Length; i++)
                {
                    xlWorkSheet.Cells[1, i + 1] = headerAllPoints[i];
                }
                for (var i = 0; i < allPoints.Count; i++)
                {
                    xlWorkSheet.Cells[i + 2, 1] = allPoints[i].Line;
                    xlWorkSheet.Cells[i + 2, 2] = allPoints[i].Station;
                    xlWorkSheet.Cells[i + 2, 3] = allPoints[i].Robot;
                    xlWorkSheet.Cells[i + 2, 4] = allPoints[i].ProgramNo;
                    xlWorkSheet.Cells[i + 2, 5] = allPoints[i].PointName;
                    xlWorkSheet.Cells[i + 2, 6] = allPoints[i].PTP_LIN1;
                    xlWorkSheet.Cells[i + 2, 7] = allPoints[i].CONT1;
                    xlWorkSheet.Cells[i + 2, 8] = allPoints[i].Vel1;
                    xlWorkSheet.Cells[i + 2, 9] = allPoints[i].ToolNo;
                    xlWorkSheet.Cells[i + 2, 10] = allPoints[i].ToolName;
                    xlWorkSheet.Cells[i + 2, 11] = allPoints[i].BaseNo;
                    xlWorkSheet.Cells[i + 2, 12] = allPoints[i].BaseName;
                    xlWorkSheet.Cells[i + 2, 13] = allPoints[i].ExtTCP;
                    xlWorkSheet.Cells[i + 2, 14] = allPoints[i].PDAT_LDAT1;
                    xlWorkSheet.Cells[i + 2, 15] = allPoints[i].FDAT1;
                    xlWorkSheet.Cells[i + 2, 16] = allPoints[i].E6POS1;
                    xlWorkSheet.Cells[i + 2, 17] = allPoints[i].GlobalType;
                    xlWorkSheet.Cells[i + 2, 18] = allPoints[i].ExtTCP;
                    xlWorkSheet.Cells[i + 2, 19] = allPoints[i].X_A11;
                    xlWorkSheet.Cells[i + 2, 20] = allPoints[i].Y_A21;
                    xlWorkSheet.Cells[i + 2, 21] = allPoints[i].Z_A31;
                    xlWorkSheet.Cells[i + 2, 23] = allPoints[i].A_A41;
                    xlWorkSheet.Cells[i + 2, 24] = allPoints[i].B_A51;
                    xlWorkSheet.Cells[i + 2, 25] = allPoints[i].C_A61;
                    xlWorkSheet.Cells[i + 2, 26] = allPoints[i].S1;
                    xlWorkSheet.Cells[i + 2, 27] = allPoints[i].T1;
                    xlWorkSheet.Cells[i + 2, 28] = allPoints[i].E11;
                    xlWorkSheet.Cells[i + 2, 29] = allPoints[i].E21;
                    xlWorkSheet.Cells[i + 2, 30] = allPoints[i].E31;
                    xlWorkSheet.Cells[i + 2, 31] = allPoints[i].E41;
                    xlWorkSheet.Cells[i + 2, 32] = allPoints[i].E51;
                    xlWorkSheet.Cells[i + 2, 33] = allPoints[i].E61;
                    xlWorkSheet.Cells[i + 2, 34] = allPoints[i].Tool1;
                    xlWorkSheet.Cells[i + 2, 35] = allPoints[i].Base1;
                    xlWorkSheet.Cells[i + 2, 36] = allPoints[i].IpoFrame;
                    xlWorkSheet.Cells[i + 2, 37] = allPoints[i].Vel2;
                    xlWorkSheet.Cells[i + 2, 38] = allPoints[i].Acc;
                    xlWorkSheet.Cells[i + 2, 39] = allPoints[i].ApoDiststring;
                    xlWorkSheet.Cells[i + 2, 40] = allPoints[i].ApoModestring;
                    xlWorkSheet.Cells[i + 2, 41] = allPoints[i].GearJerkstring;
                    xlWorkSheet.Cells[i + 2, 42] = allPoints[i].Expax;
                    xlWorkSheet.Cells[i + 2, 43] = allPoints[i].Apo_Facstring;
                    xlWorkSheet.Cells[i + 2, 44] = allPoints[i].AxisVel;
                    xlWorkSheet.Cells[i + 2, 45] = allPoints[i].AxisAcc;

                }
                xlWorkSheet.Columns.AutoFit();
                if (numOfSWPFiles > 1)
                {
                    //SWP
                    xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(2);
                    xlWorkSheet.Activate();
                    xlWorkSheet.Name = "SpotWelding";


                    //Headers definition:
                    Excel.Range rangeHeader = xlWorkSheet.get_Range("a1", "o1");
                    Excel.Range greenHeader = xlWorkSheet.get_Range("a1", "h1");
                    Excel.Range yellowHeader = xlWorkSheet.get_Range("i1", "k1");
                    Excel.Range orangeHeader = xlWorkSheet.get_Range("l1", "p1");

                    //Headers formating: color
                    greenHeader.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#C6E0B4");
                    yellowHeader.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#FFE699");
                    orangeHeader.Interior.Color = System.Drawing.ColorTranslator.FromHtml("#F8CBAD");


                    //
                    //Line	Station	Robot	Program No	Point No	X	Y	Z	HDL X	HDL Y	HDL Z	delta X	delta Y	delta Z wektor XYZ Comment
                    //Header:
                    xlWorkSheet.Cells[1, 1] = "Line";
                    xlWorkSheet.Cells[1, 2] = "Station";
                    xlWorkSheet.Cells[1, 3] = "Robot";
                    xlWorkSheet.Cells[1, 4] = "Program No";
                    xlWorkSheet.Cells[1, 5] = "Point No";
                    xlWorkSheet.Cells[1, 6] = "X";
                    xlWorkSheet.Cells[1, 7] = "Y";
                    xlWorkSheet.Cells[1, 8] = "Z";
                    xlWorkSheet.Cells[1, 9] = "HDL X";
                    xlWorkSheet.Cells[1, 10] = "HDL Y";
                    xlWorkSheet.Cells[1, 11] = "HDL Z";
                    xlWorkSheet.Cells[1, 12] = "delta X";
                    xlWorkSheet.Cells[1, 13] = "delta Y";
                    xlWorkSheet.Cells[1, 14] = "delta Z";
                    xlWorkSheet.Cells[1, 15] = "wektor XYZ";
                    xlWorkSheet.Cells[1, 16] = "Comment";

                    //Set autofilter to range A1:P1 and set Horizontal & Vertical Align to center

                    Excel.Range firstRow = xlWorkSheet.get_Range("a1", "p1");
                    firstRow.Style.HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                    firstRow.Style.VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    firstRow.Activate();
                    firstRow.Select();
                    firstRow.AutoFilter(1, Type.Missing, Excel.XlAutoFilterOperator.xlFilterValues, Type.Missing, true);


                    Excel.Range decimalFormat;
                    decimalFormat = xlWorkSheet.Columns["F:O"];
                    decimalFormat.NumberFormat = "#,##0.00";


                    // Conditional formating HDL columns
                    // if value >5 color RED
                    // if value is betwen 3-5 YELLOW
                    // below WHITE

                    Excel.Range range = xlWorkSheet.Columns["L:O"];
                    Excel.FormatConditions fcs = range.FormatConditions;

                    Excel.FormatCondition fcNormal = (Excel.FormatCondition)fcs.Add(Excel.XlFormatConditionType.xlCellValue, Excel.XlFormatConditionOperator.xlLess, "=3");
                    Excel.Interior interiorN = fcNormal.Interior;
                    interiorN.Color = System.Drawing.ColorTranslator.FromHtml("#FFFFFF"); //white

                    Excel.FormatCondition fcWarning = (Excel.FormatCondition)fcs.Add(Excel.XlFormatConditionType.xlCellValue, Excel.XlFormatConditionOperator.xlLessEqual, "=5");
                    Excel.Interior interiorW = fcWarning.Interior;
                    interiorW.Color = System.Drawing.ColorTranslator.FromHtml("#FFCC00"); //yellow

                    Excel.FormatCondition fcDanger = (Excel.FormatCondition)fcs.Add(Excel.XlFormatConditionType.xlCellValue, Excel.XlFormatConditionOperator.xlGreater, "=5");
                    Excel.Interior interior = fcDanger.Interior;
                    interior.Color = System.Drawing.ColorTranslator.FromHtml("#CC0000"); //red

                    //Data from :
                    //dataTo.ForEach(dt=>Console.WriteLine(dt.robot));
                    for (int i = 0; i < DataTo.Count; i++)
                    {
                        xlWorkSheet.Cells[i + 2, 1] = DataTo[i].line.ToString();
                        xlWorkSheet.Cells[i + 2, 2] = DataTo[i].station.ToString();
                        xlWorkSheet.Cells[i + 2, 3] = DataTo[i].robot.ToString();
                        xlWorkSheet.Cells[i + 2, 4] = DataTo[i].programNo.ToString();
                        xlWorkSheet.Cells[i + 2, 5] = DataTo[i].pointNo.ToString();
                        xlWorkSheet.Cells[i + 2, 6] = DataTo[i].pointX;
                        xlWorkSheet.Cells[i + 2, 7] = DataTo[i].pointY;
                        xlWorkSheet.Cells[i + 2, 8] = DataTo[i].pointZ;
                        xlWorkSheet.Cells[i + 2, 9] = DataTo[i].pointHDL_X;
                        xlWorkSheet.Cells[i + 2, 10] = DataTo[i].pointHDL_Y;
                        xlWorkSheet.Cells[i + 2, 11] = DataTo[i].pointHDL_Z;
                        xlWorkSheet.Cells[i + 2, 12].Formula = "=SQRT((F" + (i + 2) + "-I" + (i + 2) + ")*" + "(F" + (i + 2) + "-I" + (i + 2) + "))";
                        xlWorkSheet.Cells[i + 2, 13].Formula = "=SQRT((G" + (i + 2) + "-J" + (i + 2) + ")*" + "(G" + (i + 2) + "-J" + (i + 2) + "))";
                        xlWorkSheet.Cells[i + 2, 14].Formula = "=SQRT((H" + (i + 2) + "-K" + (i + 2) + ")*" + "(H" + (i + 2) + "-K" + (i + 2) + "))";
                        xlWorkSheet.Cells[i + 2, 15].Formula = "=ABS(SQRT(POWER(L" + (i + 2) + "+M" + (i + 2) + "+N" + (i + 2) + ",2)))";
                        xlWorkSheet.Cells[i + 2, 16] = DataTo[i].Comment;
                    }
                    //Set Autofit to all columns
                    xlWorkSheet.Columns.AutoFit();
                }



                //Save file
                string timeStamp = DateTime.Now.ToString("ddMMyyyyHmmss");
                xlWorkBook.SaveAs(actualDirectory + "Checpoints-Excel_" + timeStamp + ".xlsx", Excel.XlFileFormat.xlOpenXMLWorkbook, misValue, misValue, misValue, misValue, Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                Marshal.ReleaseComObject(xlWorkSheet);
                Marshal.ReleaseComObject(xlWorkBook);
                Marshal.ReleaseComObject(xlApp);


                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Kod błedu: " + e.ToString());
                return false;
            }
        }

        public bool extractFile()
        {
            // MessageBox.Show("Pliki do wypakowania: " + this.filesPath, "extractFile");
            if (checkDirectoryToExtract(extractDirectory) == true)
            {
                //    MessageBox.Show("filespath:" + filesPath + "\n extractPath: " + extractDirectory, "extractFile");
                ZipFile.ExtractToDirectory(filesPath, extractDirectory);
                return true;
            }
            else
            {
                return false;
            }
        }
        private bool chcekDirectoryExistAndNoEmpty(string dirPath)
        {
            try
            {
                if (Directory.Exists(dirPath))
                {
                    return true;
                }
                else
                {
                    DirectoryInfo directory = Directory.CreateDirectory(dirPath);
                    MessageBox.Show("Błąd!, Brak plików HLD w folderze: " + dirPath + "\\hdl");
                    return false;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Bład przy poszukiwaniu pliku HDL: " + e.ToString());
                return false;
            }


        }
        private bool checkDirectoryToExtract(string path)
        {
            try
            {

                if (Directory.Exists(path))
                {
                    //   MessageBox.Show("sprawdzam czy scieżka istnieje: " + path);
                    //if exist delete files and folder to empty the folder
                    deleteOldFiles(path);
                    deleteOldFiles(path);
                    return true;
                }
                else
                {
                    // or create folder
                    DirectoryInfo directory = Directory.CreateDirectory(path);
                    return true;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Nie mogę utorzyć folderu w: " + path);
                MessageBox.Show("Kod błedu: " + e.ToString());
                return false;
            }

        }
        private void deleteOldFiles(string path)
        {
            DirectoryInfo di = new DirectoryInfo(path);

            foreach (FileInfo file in di.GetFiles())
            {
                //MessageBox.Show(file.ToString());
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                //MessageBox.Show(dir.ToString());
                dir.Delete(true);
            }
        }
    }
}
