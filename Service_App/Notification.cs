﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_App
{
    class Notification
    {
        private string textNotification;

        public Notification()
        {

        }

        public string TextNotification
        {
            get
            {
                return textNotification;
            }

            set
            {
                textNotification = value;
            }
        }
        public void updateText(string text)
        {
            TextNotification += "\n"+ text;

        }
    }
}
