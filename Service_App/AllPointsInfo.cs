﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service_App
{
    class AllPointsInfo
    {
        string line;
        string station;
        int robot;
        string programNo;
        string pointName;
        string PTP_LIN;
        string CONT;
        string vel;
        string toolNo;
        string toolName;
        string baseNo;
        string baseName;
        string extTCP;
        string PDAT_LDAT;
        string FDAT;
        string E6POS;
        string globalType;
        string X_A1;
        string Y_A2;
        string Z_A3;
        string A_A4;
        string B_A5;
        string C_A6;
        string S;
        string T;
        string E1, E2, E3, E4, E5, E6;
        string Tool;
        string ipoFrame;
        string Base;
        string Vel;
        string acc;
        string apoDiststring;
        string apoModestring;
        string gearJerkstring;
        string expax;
        string ign;
        string apo_Facstring;
        string axisVel;
        string axisAcc;
        string commentOptimization;
        string refreshPointstring;


        public AllPointsInfo CreateCopy()
        {
            return (AllPointsInfo)this.MemberwiseClone();
        }

        public string Line
        {
            get
            {
                return line;
            }

            set
            {
                line = value;
            }
        }

        public string Station
        {
            get
            {
                return station;
            }

            set
            {
                station = value;
            }
        }

        public int Robot
        {
            get
            {
                return robot;
            }

            set
            {
                robot = value;
            }
        }

        public string ProgramNo
        {
            get
            {
                return programNo;
            }

            set
            {
                programNo = value;
            }
        }

        public string PointName
        {
            get
            {
                return pointName;
            }

            set
            {
                pointName = value;
            }
        }

        public string PTP_LIN1
        {
            get
            {
                return PTP_LIN;
            }

            set
            {
                PTP_LIN = value;
            }
        }

        public string CONT1
        {
            get
            {
                return CONT;
            }

            set
            {
                CONT = value;
            }
        }

        public string Vel1
        {
            get
            {
                return vel;
            }

            set
            {
                vel = value;
            }
        }

        public string ToolNo
        {
            get
            {
                return toolNo;
            }

            set
            {
                toolNo = value;
            }
        }

        public string ToolName
        {
            get
            {
                return toolName;
            }

            set
            {
                toolName = value;
            }
        }

        public string BaseNo
        {
            get
            {
                return baseNo;
            }

            set
            {
                baseNo = value;
            }
        }

        public string BaseName
        {
            get
            {
                return baseName;
            }

            set
            {
                baseName = value;
            }
        }

        public string ExtTCP
        {
            get
            {
                return extTCP;
            }

            set
            {
                extTCP = value;
            }
        }

        public string PDAT_LDAT1
        {
            get
            {
                return PDAT_LDAT;
            }

            set
            {
                PDAT_LDAT = value;
            }
        }

        public string FDAT1
        {
            get
            {
                return FDAT;
            }

            set
            {
                FDAT = value;
            }
        }

        public string E6POS1
        {
            get
            {
                return E6POS;
            }

            set
            {
                E6POS = value;
            }
        }

        public string GlobalType
        {
            get
            {
                return globalType;
            }

            set
            {
                globalType = value;
            }
        }

        public string X_A11
        {
            get
            {
                return X_A1;
            }

            set
            {
                X_A1 = value;
            }
        }

        public string Y_A21
        {
            get
            {
                return Y_A2;
            }

            set
            {
                Y_A2 = value;
            }
        }

        public string Z_A31
        {
            get
            {
                return Z_A3;
            }

            set
            {
                Z_A3 = value;
            }
        }

        public string A_A41
        {
            get
            {
                return A_A4;
            }

            set
            {
                A_A4 = value;
            }
        }

        public string B_A51
        {
            get
            {
                return B_A5;
            }

            set
            {
                B_A5 = value;
            }
        }

        public string C_A61
        {
            get
            {
                return C_A6;
            }

            set
            {
                C_A6 = value;
            }
        }

        public string S1
        {
            get
            {
                return S;
            }

            set
            {
                S = value;
            }
        }

        public string T1
        {
            get
            {
                return T;
            }

            set
            {
                T = value;
            }
        }

        public string E11
        {
            get
            {
                return E1;
            }

            set
            {
                E1 = value;
            }
        }

        public string E21
        {
            get
            {
                return E2;
            }

            set
            {
                E2 = value;
            }
        }

        public string E31
        {
            get
            {
                return E3;
            }

            set
            {
                E3 = value;
            }
        }

        public string E41
        {
            get
            {
                return E4;
            }

            set
            {
                E4 = value;
            }
        }

        public string E51
        {
            get
            {
                return E5;
            }

            set
            {
                E5 = value;
            }
        }

        public string E61
        {
            get
            {
                return E6;
            }

            set
            {
                E6 = value;
            }
        }

        public string Tool1
        {
            get
            {
                return Tool;
            }

            set
            {
                Tool = value;
            }
        }

        public string IpoFrame
        {
            get
            {
                return ipoFrame;
            }

            set
            {
                ipoFrame = value;
            }
        }

        public string Base1
        {
            get
            {
                return Base;
            }

            set
            {
                Base = value;
            }
        }

        public string Vel2
        {
            get
            {
                return Vel;
            }

            set
            {
                Vel = value;
            }
        }

        public string Acc
        {
            get
            {
                return acc;
            }

            set
            {
                acc = value;
            }
        }

        public string ApoDiststring
        {
            get
            {
                return apoDiststring;
            }

            set
            {
                apoDiststring = value;
            }
        }

        public string ApoModestring
        {
            get
            {
                return apoModestring;
            }

            set
            {
                apoModestring = value;
            }
        }

        public string GearJerkstring
        {
            get
            {
                return gearJerkstring;
            }

            set
            {
                gearJerkstring = value;
            }
        }

        public string Expax
        {
            get
            {
                return expax;
            }

            set
            {
                expax = value;
            }
        }

        public string Ign
        {
            get
            {
                return ign;
            }

            set
            {
                ign = value;
            }
        }

        public string Apo_Facstring
        {
            get
            {
                return apo_Facstring;
            }

            set
            {
                apo_Facstring = value;
            }
        }

        public string AxisVel
        {
            get
            {
                return axisVel;
            }

            set
            {
                axisVel = value;
            }
        }

        public string AxisAcc
        {
            get
            {
                return axisAcc;
            }

            set
            {
                axisAcc = value;
            }
        }

        public string CommentOptimization
        {
            get
            {
                return commentOptimization;
            }

            set
            {
                commentOptimization = value;
            }
        }

        public string RefreshPointstring
        {
            get
            {
                return refreshPointstring;
            }

            set
            {
                refreshPointstring = value;
            }
        }

        public AllPointsInfo()
        {

        }
    }
}
